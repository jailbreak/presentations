---
title: Jailbreak 2021-08-20 - Atelier catalogue de données
separator: <!--s-->
verticalSeparator: <!--v-->
theme: simple
revealOptions:
    transition: 'slide'
---

## Catalogue de données

Groupe de travail Schéma

Atelier 20 août 2021

<!--s-->

# Introduction

- Tour de table
- Point d'étape sur l'investigation en cours
- But de cet atelier

<!--s-->

## Quelques notions

- Schéma
- Catalogue de données
- Métadonnées
- Métadonnées d'un catalogue  
("Quel est le producteur de ce catalogue ?")
- Métadonnées des données cataloguées  
("Quel est le producteur de ce jeu de données ?")

<!--s-->

## Ce qui existe déjà

<!--v-->

- [Arrêté sur les données de référence](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000034944648/) (2017)
- [INSPIRE](http://inspire.ec.europa.eu/documents/Metadata/MD_IR_and_ISO_20131029.pdf)
- [DCAT-AP](https://raw.githubusercontent.com/SEMICeu/DCAT-AP/master/releases/2.0.1/DCAT_AP_2.0.1.pdf)
- [Schéma OpenDataFrance](https://scdl.opendatafrance.net/docs/schemas/catalogue.html)
- Schémas internes

<!--v-->

## Comparaison

https://lite.framacalc.org/9p8z-schema_catalogue_donnees

<!--s-->

## Discussion

<!--s-->

## Prochaines étapes